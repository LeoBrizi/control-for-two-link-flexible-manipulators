# Control for two-link flexible manipulators 

This project wants to replicate the results obtained in the [paper](https://www.sciencedirect.com/science/article/pii/S147466701631535X).
The paper presents a dynamic model and a class of terminal sliding mode control (TSMC) for two-link flexible manipulators with non-collocated feedback. The proposed TSMC guarantees that the output tracking error converges to zero in finite time under external disturbance.