%simulation with pid controller
clear;
clc;
%simulation parameters
tot_time = 10;
dt = 0.001;

%white noise parameters
noise_band = 0.02;

%initial condition
init_state = [0.4; 0.6];

%desidered trajectory
theta_d_traj = @(t) [sin(t); cos(t)];

%PD parameters
K_p = 200;
K_d = 60;

elapsed_time = 0;
robot = FlexManipulator(init_state);

%stuff for plots
joint_angles = [];
desidered_traj = [];
errors = [];
control_torques = [];
time = [];


while elapsed_time <= tot_time
    %compute the desired position
    theta_d = theta_d_traj(elapsed_time);
    desidered_traj(:,end+1) = theta_d;
    
    %get the robot state
    [theta, theta_dot] = robot.GetState();
    joint_angles(:,end+1) = theta;
    
    %compute the control
    errors(:,end+1) = theta - theta_d;
    tau_PD = PDController(theta, theta_dot, theta_d, K_p, K_d);
    control_torques(:,end+1) = tau_PD;
    
    %add noise
    disturbances = normrnd(0, noise_band, [2,1]); %[LEO] possible error
    tau_PD = tau_PD + disturbances;
    
    %apply the control
    time(end+1) = elapsed_time;
    robot.Integrate(tau_PD, dt);
    elapsed_time = elapsed_time + dt;
end



%% plots

%Joint angle for link 1 under PD control
clf;
plot(time, joint_angles(1,:), time, desidered_traj(1,:));
grid minor;
ylabel('Angle[rad]');
xlabel('Time[s]');
ylim([-1.5 1.5])
legend("true trajectory", "desired trajectory");
title("Joint angle for link 1");
saveas(gca, 'plots/Joint_angle_1_PD_control.png');

%Joint angle for link 2 under PD control
clf;
plot(time, joint_angles(2,:), time, desidered_traj(2,:));
grid minor;
ylabel('Angle[rad]');
xlabel('Time[s]');
ylim([-1.5 1.5])
legend("true trajectory", "desired trajectory");
title("Joint angle for link 2");
saveas(gca, 'plots/Joint_angle_2_PD_control.png');

%Tracking errors under PD control
clf;
plot(time, errors(1,:), time, errors(2,:));
grid minor;
ylabel('Angle[rad]');
xlabel('Time[s]');
legend("Error for link 1", "Error for link 2");
title("Errors of joint angle");
saveas(gca, 'plots/Tracking_errors_PD_control.png');

%Control torque under PD control
clf;
plot(time, control_torques(1,:), time, control_torques(2,:));
grid minor;
ylabel('Control Torque[Nm]');
xlabel('Time[s]');
legend("tol1", "tol2");
title("Control effort");
saveas(gca, 'plots/Control_torque_PD_control.png');
