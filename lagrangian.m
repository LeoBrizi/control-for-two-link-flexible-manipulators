syms t theta_1(t) theta_2(t) delta_11(t) delta_12(t) delta_21(t) delta_22(t)
syms theta_1_d(t) theta_2_d(t) delta_11_d(t) delta_12_d(t) delta_21_d(t) delta_22_d(t)
syms l1 l2 m1 m2 EI1 EI2 M1 M2

q = [theta_1; theta_2; delta_11; delta_12; delta_21; delta_22];
q_d = [theta_1_d; theta_2_d; delta_11_d; delta_12_d; delta_21_d; delta_22_d];

T1 = 1/6*m1*l1^2*theta_1_d^2 + 1/4*m1*delta_11^2*theta_1_d^2 + 1/4*m1*delta_12^2*theta_1_d^2 ...
     + 1/4*m1*delta_11_d^2 + 1/4*m1*delta_12_d^2 - 1/pi*m1*l1*theta_1_d*delta_11_d + 1/(2*pi)*m1*l1*theta_1_d*delta_12_d;

T2 = 1/2*m2*l1^2*theta_1_d^2 + 1/6*m2*l2^2*theta_2_d^2 + 1/4*m2*delta_21^2*theta_2_d^2 ...
     + 1/4*m2*delta_22^2*theta_2_d^2 + 1/4*m2*delta_21_d^2 + 1/4*m2*delta_22_d^2 + 1/2*m2*l1*l2*theta_1_d*theta_2_d*cos(theta_1 - theta_2) ...
     + 2/pi*m2*l1*theta_1_d*delta_21_d*cos(theta_1 - theta_2) + 2/pi*m2*l1*delta_21*theta_1_d*theta_2_d*sin(theta_1-theta_2) ...
     + 1/pi*m2*l2*theta_2_d*delta_21_d - 1/(2*pi)*m2*l2*theta_2_d*delta_22_d;

T3 = 1/2*M1*l1*theta_1_d^2;
T4 = 1/2*M2*l1^2*theta_1_d^2 + 1/2*M2*l2^2*theta_2_d^2 + M2*l1*l2*theta_1_d*theta_2_d*cos(theta_1-theta_2);

T = T1 + T2 + T3 + T4;

U = 1/2*EI1*((pi^4/(2*l1^3))*delta_11^2 + (8*pi^4/l1^3)*delta_12^2) + 1/2*EI2*((pi^4/(2*l2^3))*delta_21^2 + (8*pi^4/l2^3)*delta_22^2);

L = T-U;

dL_dq = functionalDerivative(L, q);
dL_dq_d = functionalDerivative(L, q_d);
droba_dt = diff(dL_dq_d, t);

EL_eq = droba_dt - dL_dq;
display(EL_eq);
