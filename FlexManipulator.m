classdef FlexManipulator < matlab.mixin.Copyable
    %FlexManipulator is a class for modeling the flexible two-link
    %manipulator.
    
    properties(Access = private)
        q_theta;    %[theta_1; theta_2] stiffness generalized vector
        q_delta;    %[delta_11; delta_12; delta_21; delta_22] flexible generalized vector
        q_theta_dot;
        q_delta_dot;
        l1;         %length link 1
        l2;         %length link 2
        m1;         %mass link 1
        m2;         %mass link 2
        EI1;        %flexural rigidity of link 1
        EI2;        %flexural rigidity of link 2
        M1;         %mass of the motor 2
        M2;         %mass of the load
        time;
    end
    
    methods
        function obj = FlexManipulator(q_theta)
            %FlexManipulator Construct an instance of this class
            obj.q_theta = q_theta;
            obj.q_delta = zeros(4,1);
            obj.q_theta_dot = zeros(2,1);
            obj.q_delta_dot = zeros(4,1);
            obj.l1 = 1.0;
            obj.l2 = 1.0;
            obj.m1 = 2.0;
            obj.m2 = 2.0;
            obj.EI1 = 2.0;
            obj.EI2 = 2.0;
            obj.M1 = 0.8;
            obj.M2 = 0.5;
            obj.time = 0;
        end
        
        function Integrate(obj, tau_theta, step_time)
            %Integrate itegrates the dynamic system in order to simulate 
            %the state evolution
            %PARAMETERS:
            %tau_theta: input vector 
            %step_time: integration time
            %SIDE EFFECTS: Update q, q_d variables according to generated motion due to tau
            
            function x_dot = dynamic(t, x)
                % x = [q;q_dot]
                % x_dot = [q_dot;q_dot_dot]
                
                obj.q_theta = wrapToPi(x(1:2));
                obj.q_delta = x(3:6);
                obj.q_theta_dot = x(7:8);
                obj.q_delta_dot = x(9:12);
                
                M = obj.GetNewM();
                C = obj.GetNewC();
                K = obj.GetK();
                K_q = zeros(6,6);
                K_q(3:6,3:6) = K;
                
                q_dot_dot = inv(M)*([tau_theta; zeros(4,1)] - C - K_q*[obj.q_theta; obj.q_delta]);

                x_dot = [obj.q_theta_dot; obj.q_delta_dot; q_dot_dot];
            end
            
            [t, x] = ode45(@dynamic, [obj.time obj.time + step_time], [obj.q_theta;obj.q_delta;obj.q_theta_dot;obj.q_delta_dot]);
            obj.time = t(end);
            x = x(end,:)';
            obj.q_theta = wrapToPi(x(1:2));
            obj.q_delta = x(3:6);
            obj.q_theta_dot = x(7:8);
            obj.q_delta_dot = x(9:12);
            
            %% Euler integration
%             M = obj.GetNewM();
%             C = obj.GetNewC();
%             K = obj.GetK();
%             K_q = zeros(6,6);
%             K_q(3:6,3:6) = K;
%             
%             q_dot_dot = inv(M)*([tau_theta; zeros(4,1)] - C - K_q*[obj.q_theta; obj.q_delta]);
%             obj.q_theta_dot = obj.q_theta_dot + step_time*q_dot_dot(1:2);
%             obj.q_delta_dot = obj.q_delta_dot + step_time*q_dot_dot(3:6);
%             obj.q_theta = obj.q_theta + step_time*obj.q_theta_dot;
%             obj.q_delta = obj.q_delta + step_time*obj.q_delta_dot;
            
        end
        
        function [q_theta, q_theta_dot] = GetState(obj)
            q_theta = obj.q_theta;
            q_theta_dot = obj.q_theta_dot;
        end
    end
    
    methods(Access = private)
        
        function M = GetM(obj)
            %GetM Computes the inertia matrix of the system for the current
            %configuration
            m11 = (1/3*obj.m1 + obj.m2 + obj.M1 + obj.M2)*obj.l1^2 + 1/2*obj.m1*(obj.q_delta(1)^2 + obj.q_delta(2)^2);
            m12 = (1/2*obj.m1 + obj.M1)*obj.l1*obj.l2*cos(obj.q_theta(1) - obj.q_theta(2)) + 2/pi*obj.m2*obj.l1*obj.q_delta(3)*sin(obj.q_theta(1) - obj.q_theta(2));
            m13 = -1/pi*obj.m1*obj.l1;
            m14 = 1/(2*pi)*obj.m1*obj.l1;
            m15 = 2/pi*obj.m2*obj.l1*cos(obj.q_theta(1) - obj.q_theta(2));
            m22 = (1/3*obj.m2 + obj.M2)*obj.l2^2 + 1/2*obj.m2*(obj.q_delta(3)^2 + obj.q_delta(4)^2);
            m25 = 1/pi*obj.m2*obj.l2;
            m26 = -1/(2*pi)*obj.m2*obj.l2;
            m33 = 1/2*obj.m1;
            m44 = 1/2*obj.m1;
            m55 = 1/2*obj.m2;
            m66 = 1/2*obj.m2;
            
            M = [m11, m12, m13, m14, m15,   0;
                 m12, m22,   0,   0, m25, m26;
                 m13,   0, m33,   0,   0,   0;
                 m14,   0,   0, m44,   0,   0;
                 m15, m25,   0,   0, m55,   0;
                   0, m26,   0,   0,   0, m66];
        end
        
        function C = GetC(obj)
            %GetC Computes the Coriolis-centrifugal force in the current
            %state
            c1 = 1/2*(obj.m2 - obj.m1)*obj.l1*obj.l2*obj.q_theta_dot(1)*obj.q_theta_dot(2)*sin(obj.q_theta(1) - obj.q_theta(2)) + 1/2*(obj.m1 + obj.M2)*obj.l1*obj.l2*obj.q_theta_dot(2)^2*sin(obj.q_theta(1) - obj.q_theta(2))...
                 + 4/pi*obj.m2*obj.l1*obj.q_theta_dot(2)*obj.q_delta_dot(3)*sin(obj.q_theta(1) - obj.q_theta(2)) - 2/pi*obj.m2*obj.l1*obj.q_delta(3)*obj.q_theta_dot(2)^2*cos(obj.q_theta(1) - obj.q_theta(2))...
                 + obj.m1*obj.q_delta(1)*obj.q_theta_dot(1)*obj.q_delta_dot(1) + obj.m1*obj.q_delta(2)*obj.q_theta_dot(1)*obj.q_delta_dot(2);
            c2 = -1/2*(obj.m2 + obj.M2)*obj.l1*obj.l2*obj.q_theta_dot(1)^2*sin(obj.q_theta(1) - obj.q_theta(2)) + 2/pi*obj.m2*obj.l1*obj.q_delta(3)*obj.q_theta_dot(1)^2*cos(obj.q_theta(1) - obj.q_theta(2))...
                 + obj.m2*obj.q_delta(3)*obj.q_theta_dot(2)*obj.q_delta_dot(3) + obj.m2*obj.q_delta(4)*obj.q_theta_dot(2)*obj.q_delta_dot(4);
            c3 = -1/2*obj.m1*obj.q_delta(1)*obj.q_theta_dot(1);
            c4 = -1/2*obj.m1*obj.q_delta(2)*obj.q_theta_dot(1);
            c5 = -2/pi*obj.m2*obj.l1*obj.q_theta_dot(1)^2*sin(obj.q_theta(1) - obj.q_theta(2)) - 1/2*obj.m2*obj.q_delta(3)*obj.q_theta_dot(2)^2;
            c6 = -1/2*obj.m2*obj.q_delta(4)*obj.q_theta_dot(2);
            C = [c1; c2; c3; c4; c5; c6];
        end
        
        function K = GetK(obj)
            %GetK Computes the stiffness matrix
            K = zeros(4);
            K(1,1) = (pi^4)/(2*obj.l1^3)*obj.EI1;
            K(2,2) = (8*pi^4)/(obj.l1^3)*obj.EI1;
            K(3,3) = (pi^4)/(2*obj.l2^3)*obj.EI2;
            K(4,4) = (8*pi^4)/(obj.l2^3)*obj.EI2;
        end
        
        function M = GetNewM(obj)
            
            m11 = (obj.M2 + obj.m1/3 + obj.m2 + obj.M1)*obj.l1^2 + (obj.q_delta(1)^2 + obj.q_delta(2)^2)*obj.m1/2;
            m12 = (obj.M2 + obj.m2/2)*obj.l1*obj.l2*cos(obj.q_theta(1) - obj.q_theta(2)) + 2/pi*obj.l1*obj.m2*sin(obj.q_theta(1) - obj.q_theta(2))*obj.q_delta(3);
            m13 = -1/pi*obj.l1*obj.m1;
            m14 = 1/(2*pi)*obj.l1*obj.m1;
            m15 = 2/pi*obj.l1*obj.m2*cos(obj.q_theta(1) - obj.q_theta(2));
            m22 = (obj.M2 + obj.m2/3)*obj.l2^2 + (obj.q_delta(3) + obj.q_delta(4))*obj.m2/2;
            m25 = 1/pi*obj.l2*obj.m2;
            m26 = -1/(2*pi)*obj.l2*obj.m2;
            m33 = obj.m1/2;
            m44 = obj.m1/2;
            m55 = obj.m2/2;
            m66 = obj.m2/2;
            
            M = [m11, m12, m13, m14, m15,   0;
                 m12, m22,   0,   0, m25, m26;
                 m13,   0, m33,   0,   0,   0;
                 m14,   0,   0, m44,   0,   0;
                 m15, m25,   0,   0, m55,   0;
                   0, m26,   0,   0,   0, m66];
        end
        
        function C = GetNewC(obj)
            c1 = obj.m1*obj.q_delta(1)*obj.q_theta_dot(1)*obj.q_delta_dot(1) + obj.m1*obj.q_delta(2)*obj.q_theta_dot(1)*obj.q_delta_dot(2) - (2*obj.l1*obj.m2*sin(obj.q_theta(1) - obj.q_theta(2))*obj.q_delta_dot(3)*(obj.q_theta_dot(1) - obj.q_theta_dot(2)))/pi ...
                + (2*obj.l1*obj.m2*sin(obj.q_theta(1) - obj.q_theta(2))*obj.q_theta_dot(2)*obj.q_delta_dot(3))/pi + (2*obj.l1*obj.m2*sin(obj.q_theta(1) - obj.q_theta(2))*obj.q_delta_dot(3)*obj.q_theta_dot(1))/pi + (2*obj.l1*obj.m2*obj.q_delta(3)*obj.q_theta_dot(2)*cos(obj.q_theta(1) - obj.q_theta(2))*(obj.q_theta_dot(1) - obj.q_theta_dot(2)))/pi...
                + obj.M2*obj.l1*obj.l2*sin(obj.q_theta(1) - obj.q_theta(2))*obj.q_theta_dot(1)*obj.q_theta_dot(2) + (obj.l1*obj.l2*obj.m2*sin(obj.q_theta(1) - obj.q_theta(2))*obj.q_theta_dot(1)*obj.q_theta_dot(2))/2 - obj.M2*obj.l1*obj.l2*sin(obj.q_theta(1) - obj.q_theta(2))*obj.q_theta_dot(2)*(obj.q_theta_dot(1) - obj.q_theta_dot(2))...
                - (obj.l1*obj.l2*obj.m2*sin(obj.q_theta(1) - obj.q_theta(2))*obj.q_theta_dot(2)*(obj.q_theta_dot(1) - obj.q_theta_dot(2)))/2 - (2*obj.l1*obj.m2*obj.q_delta(3)*obj.q_theta_dot(1)*obj.q_theta_dot(2)*cos(obj.q_theta(1) - obj.q_theta(2)))/pi;
                      
            c2 = obj.m2*obj.q_delta(3)*obj.q_theta_dot(2)*obj.q_delta_dot(3) + obj.m2*obj.q_delta(4)*obj.q_theta_dot(2)*obj.q_delta_dot(4) + (2*obj.l1*obj.m2*sin(obj.q_theta(1) - obj.q_theta(2))*obj.q_theta_dot(1)*obj.q_delta_dot(3))/pi ...
                - (2*obj.l1*obj.m2*sin(obj.q_theta(1) - obj.q_theta(2))*obj.q_delta_dot(3)*obj.q_theta_dot(1))/pi + (2*obj.l1*obj.m2*obj.q_delta(3)*obj.q_theta_dot(1)*cos(obj.q_theta(1) - obj.q_theta(2))*(obj.q_theta_dot(1) - obj.q_theta_dot(2)))/pi ...
                - obj.M2*obj.l1*obj.l2*sin(obj.q_theta(1) - obj.q_theta(2))*obj.q_theta_dot(1)*obj.q_theta_dot(2) - (obj.l1*obj.l2*obj.m2*sin(obj.q_theta(1) - obj.q_theta(2))*obj.q_theta_dot(1)*obj.q_theta_dot(2))/2 ...
                - obj.M2*obj.l1*obj.l2*sin(obj.q_theta(1) - obj.q_theta(2))*obj.q_theta_dot(1)*(obj.q_theta_dot(1) - obj.q_theta_dot(2)) - (obj.l1*obj.l2*obj.m2*sin(obj.q_theta(1) - obj.q_theta(2))*obj.q_theta_dot(1)*(obj.q_theta_dot(1) - obj.q_theta_dot(2)))/2 ...
                + (2*obj.l1*obj.m2*obj.q_delta(3)*obj.q_theta_dot(1)*obj.q_theta_dot(2)*cos(obj.q_theta(1) - obj.q_theta(2)))/pi;
                 
            c3 = -(obj.q_delta(1)*obj.l1^3*obj.m1*obj.q_theta_dot(1)^2)/(2*obj.l1^3);
            c4 = -(obj.q_delta(2)*obj.l1^3*obj.m1*obj.q_theta_dot(1)^2)/(2*obj.l1^3);
            c5 = -(obj.l2^3*obj.m2*obj.q_delta(3)*obj.q_theta_dot(2)^2)/(2*obj.l2^3) ...
                 -(2*obj.l1*obj.l2^3*obj.m2*sin(obj.q_theta(1) - obj.q_theta(2))*obj.q_theta_dot(1)*obj.q_theta_dot(2))/(pi*obj.l2^3)...
                 -(2*obj.l1*obj.m2*sin(obj.q_theta(1) - obj.q_theta(2))*obj.q_theta_dot(1)*(obj.q_theta_dot(1) - obj.q_theta_dot(2)))/pi;
            c6 = -(obj.q_delta(4)*obj.l2^3*obj.m2*obj.q_theta_dot(2)^2)/(2*obj.l2^3);
            C = [c1; c2; c3; c4; c5; c6];
        end
        
    end
end

