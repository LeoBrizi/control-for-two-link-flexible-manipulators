function control_signal = PDController(theta, theta_dot, theta_d, K_p, K_d)
%PDController does a pd
%PARAMETERS:
%theta, theta_dot: state of the robot
%theta_d: desidered position
%K_p, K_d: control gains
%RETURNS:
%control_signal: input to apply to the robot

control_signal = -K_p*(theta - theta_d) - K_d*theta_dot;

end

